import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeViee from '../components/home.vue'
import consultore from '../components/consultorio.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'index',
    component: HomeViee
  },
  {
    path: '/consultorios',
    name: 'consulta',
    component: consultore
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
